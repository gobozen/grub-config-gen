# grub-gen-config
Grub2 mkconfig alternative for simpler distribution-specific boot menu creation/generation

separation of:
- menuentry creation: kernel image, initramfs discovery
  - may create syslinux entries too
- changing configuration - update-grub not necessary
- installing content to /boot/grub:
  - themes, backgrounds, fonts
  - translations, keyboard layouts


